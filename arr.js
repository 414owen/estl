var patch = require("./patch");
var func = require("./func");

function createRec(ind, sizes) {
	if (sizes.length === ind) {return 0;}
	var next = createRec.bind(null, ind + 1, sizes);
	return Array.create(sizes[ind], next);
}

function deepMapRec(arr, f, path) {
	var res = [];
	arr.forEach(function(a, i) {
		var p = path.append(i);
		if (Array.isArray(a)) {
			res.push(deepMapRec(a, f, p));
		} else {
			res.push(f(a, p));
		}
	});
	return res;
};

// create array from object with consecutive numeric keys from zero and .length
// eg. arguments object
patch(Array, [["from", function(a) {
	return [].slice.call(a);
}], ["create", function(sizes, elFunc) {
	var f = func.flip(elFunc || func.cnst(0));
	if (Array.isArray(sizes)) {
		return createRec(0, sizes).deepMap(f);
	}
	return Array(sizes).fill(0).map(f);
}], ["range", function(s, e, c) {
	if (!e) {e = s; s = 0;}
	var res = [];
	if (s < e)
		for (; s < e; s += (c || 1))
			res.push(s);
	else
		for (; s > e; s += (c || -1))
			res.push(s);
	return res;
}], ["repeat", function(el, times) {
	return Array.create(times, func.cnst(el));
}]]);

patch(Array.prototype, [["flatMap", function(f) {
	return this.map(f).flatten();
}], ["contains", function(a) {
	return this.indexOf(a) !== -1;
}], ["deepContains", function(a) {
	return this.deepFlatten().contains(a);
}], ["append", function(a) {
	return this.concat([].slice.call(arguments));
}], ["prepend", function(a) {
	return [].slice.call(arguments).concat(this);
}], ["deepMap", function(f) {
	return deepMapRec(this, f, []);
}], ["deepForEach", function(f) {
	this.deepMap(f);
}], ["deepReduce", function(f, acc) {
	var arr = this;

	// first element is used as accumulator by default
	if (acc === undefined) {
		acc = this[0];
		arr = this.slice(1);
	}
	arr.deepMap(function(a) {acc = f(acc, a);});
	return acc;
}], ["flatten", function () {
	return this.reduce(function(a, v) {
		return a.concat(Array.isArray(v) ? v : [v]);
	}, []);
}], ["deepFlatten", function() {
	return this.deepReduce(function(a, v) {
		return a.append(v);
	}, []);
}], ["zip", function(other) {
	var res = [];
	for (var i = 0; i < other.length && i < this.length; i++) {
		res.push([this[i], other[i]]);
	}
	return res;
}], ["zipWith", function(other, f) {
	return this.zip(other).map(function(a) {
		return f(a[0], a[1]);
	});
}], ["rotate", function(amt) {
	amt = amt % this.length;
	amt = (amt > 0 ? amt : this.length + amt);
	var rotated = this.slice(amt).concat(this.slice(0, amt));
	return rotated;
}]]);
