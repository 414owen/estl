function inc(a) {return a + 1;}
function add(a, b) {return a + b;}
function mult(a, b) {return a * b;}
function incArr(a) {return [inc(a)];}
function arr(a) {return [a];}

module.exports = {
	inc: inc,
	add: add,
	mult: mult,
	incArr: incArr,
	arr: arr
};
