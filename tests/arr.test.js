var arr = require("../arr");
var obj = require("../obj");
var common = require("./common");

/*
 * create
 */

test("Array.create(0)", () => {
	expect(Array.create(0)).toEqual([]);
});

test("Array.create(5)", () => {
	expect(Array.create(5))
		.toEqual([0, 0, 0, 0, 0]);
});

test("Array.create(3, <return path>)", () => {
	expect(Array.create(3, function(p) {
		return p;
	})).toEqual([0, 1, 2]);
});

test("Array.create([3, 4, 5])", () => {
	expect(Array.create([3, 4, 5])).toEqual([
		[[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]],
		[[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]],
		[[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
	]);
});

test("Array.create([3, 3, 2])", () => {
	expect(Array.create([3, 3, 2], function(a) {return a;})).toEqual([
		[
			[[0, 0, 0], [0, 0, 1]],
			[[0, 1, 0], [0, 1, 1]],
			[[0, 2, 0], [0, 2, 1]]
		],
		[
			[[1, 0, 0], [1, 0, 1]],
			[[1, 1, 0], [1, 1, 1]],
			[[1, 2, 0], [1, 2, 1]]
		],
		[
			[[2, 0, 0], [2, 0, 1]],
			[[2, 1, 0], [2, 1, 1]],
			[[2, 2, 0], [2, 2, 1]]
		]
	]);
});

/*
 * flatMap
 */

test("empty flatMap", () => {
	expect([].flatMap(common.inc)).toEqual([]);
});

test("two-layer flatMap", () => {
	expect([1, 2, 3, 4].flatMap(common.incArr)).toEqual([2, 3, 4, 5]);
});

test("flatMap identity is array creation from element", () => {
	expect([[1], [2, [3, 4]]].flatMap(common.arr)).toEqual([[1], [2, [3, 4]]]);
});

/*
 * contains
 */

test("empty Array.contains", () => {
	expect([].contains(0)).toBe(false);
});

test("Array.contains false", () => {
	expect([1, 2, 3, 4, 5].contains(6)).toBe(false);
});

test("Array.contains true", () => {
	expect([1, 2, 3, 4, 5].contains(5)).toBe(true);
});

/*
 * deepContains
 */

test("empty Array.deepContains", () => {
	expect([].deepContains(6)).toBe(false);
});

test("Array.deepContains false", () => {
	expect([1, 2, 3, 4, [[1, 5, 7]]].deepContains(6)).toBe(false);
});

test("Array.deepContains true", () => {
	expect([1, 2, 3, 4, [[1, 5, 7]]].deepContains(5)).toBe(true);
});

/*
 * append
 */

test("empty, single Array.append()", () => {
	expect([].append(3)).toEqual([3]);
});

test("empty, two-el Array.append()", () => {
	expect([].append(3, 4)).toEqual([3, 4]);
});

test("lots Array.append()", () => {
	expect([1, 2, 3, 4].append(5, 6, 7, 8))
		.toEqual([1, 2, 3, 4, 5, 6, 7, 8]);
});

test("nested Array.append()", () => {
	expect([1, [2, 3], 4].append([5], 6, [7, 8]))
		.toEqual([1, [2, 3], 4, [5], 6, [7, 8]]);
});

test("chained Array.append()", () => {
	expect([1, 2, 3].append(4).append(5).append(6, 7).append([8]))
		.toEqual([1, 2, 3, 4, 5, 6, 7, [8]]);
});

/*
 * prepend
 */

test("empty, single Array.prepend()", () => {
	expect([].prepend(3)).toEqual([3]);
});

test("empty, two-el Array.prepend()", () => {
	expect([].prepend(3, 4)).toEqual([3, 4]);
});

test("lots Array.prepend()", () => {
	expect([5, 6, 7, 8].prepend(1, 2, 3, 4))
		.toEqual([1, 2, 3, 4, 5, 6, 7, 8]);
});

test("nested Array.prepend()", () => {
	expect([[5], 6, [7, 8]].prepend(1, [2, 3], 4))
		.toEqual([1, [2, 3], 4, [5], 6, [7, 8]]);
});

test("chained Array.prepend()", () => {
	expect([6, 7, 8].prepend(5).prepend(4).prepend(2, 3).prepend([1]))
		.toEqual([[1], 2, 3, 4, 5, 6, 7, 8]);
});

/*
 * deepMap
 */

test("simple Array.deepMap(inc)", () => {
	expect([1, 2, 3, 4, 5, 6].deepMap(common.inc))
		.toEqual([2, 3, 4, 5, 6, 7]);
});

test("complex Array.deepMap(inc)", () => {
	expect([[[1]], [2, [3], 4], 5, 6].deepMap(common.inc))
		.toEqual([[[2]], [3, [4], 5], 6, 7]);
});

test("path in Array.deepMap()", () => {
	expect([[1, [2]], 3, [4]].deepMap(function(el, path) {
		return path;
	})).toEqual([[[0, 0], [[0, 1, 0]]], [1], [[2, 0]]]);
});

/*
 * deepForEach (same as deepMap)
 */

test("complex Array.deepMap(inc)", () => {
	var res = 0;
	[[[1]], [2, [3], 4], 5, 6].deepForEach(function(a) {res += a;});
	expect(res).toEqual(21);
});

/*
 * deepReduce
 */

test("empty Array.deepReduce(add)", () => {
	expect([].deepReduce(common.add, 0))
		.toEqual(0);
});

test("standard Array.deepReduce(add, 0)", () => {
	expect([1, 2, 3, 4, 5].deepReduce(common.add, 0))
		.toEqual(15);
});

test("complex Array.deepReduce(mult, 0)", () => {
	expect([[1], [2], [[3], [[4]], 5]].deepReduce(common.mult, 1))
		.toEqual(120);
});

test("standard Array.deepReduce(add)", () => {
	expect([1, 2, 3, 4, 5].deepReduce(common.add))
		.toEqual(15);
});

test("complex Array.deepReduce(mult)", () => {
	expect([[1], [2], [[3], [[4]], 5]].deepReduce(common.mult))
		.toEqual(120);
});

/*
 * flatten
 */

test("empty Array.flatten()", () => {
	expect([].flatten()).toEqual([]);
});

test("standard Array.flatten()", () => {
	expect([[1], [2], [3, 4, 5], [6]].flatten())
		.toEqual([1, 2, 3, 4, 5, 6]);
});

test("partial Array.flatten()", () => {
	var arr = [1, 2, 3, [4, 5], 6, 7, [8], 9, 10, 11];
	var res = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
	expect(arr.flatten()).toEqual(res);
});

test("nested Array.flatten()", () => {
	var arr = [1, 2, 3, [[[[4, 5], 6, 7, [8], 9], 10], 11]];
	var res = [1, 2, 3, [[[4, 5], 6, 7, [8], 9], 10], 11];
	expect(arr.flatten()).toEqual(res);
});

/*
 * deepFlatten
 */

test("empty Array.deepFlatten()", () => {
	expect([].deepFlatten()).toEqual([]);
});

test("complex Array.deepFlatten()", () => {
	var arr = [1, 2, 3, [[[[4, 5], 6, 7, [8], 9], 10], 11]];
	var res = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
	expect(arr.deepFlatten()).toEqual(res);
});

/*
 * peel
 */

test("empty array peel()", () => {
	expect([].peel()).toEqual([]);
});

test("peel()", () => {
	var arr = [1, function() {
		return [2, 3, function() {
			return [4, 5, 6]
		}];
	}, 7];
	var res = [1, 2, 3, 4, 5, 6, 7];
	expect(arr.peel()).toEqual(res);
});

test("peel() on number", () => {
	expect((1).peel()).toEqual([1]);
});

test("peel() on string", () => {
	expect(("Hello").peel()).toEqual(["Hello"]);
});

test("peel() on small array", () => {
	expect(([1]).peel()).toEqual([1]);
});

test("peel() on number gives right typeof", () => {
	expect(typeof((9).peel()[0])).toEqual("number");
});

test("peel() on string gives right typeof", () => {
	expect(typeof("Hello".peel()[0])).toEqual("string");
});

test("peel() on [[[string]]] gives right typeof", () => {
	expect(typeof([[["Hello"]]].peel()[0])).toEqual("string");
});

/*
 * rotate
 */

test("empty rotate()", () => {
	expect([].rotate()).toEqual([]);
});

test("single positive rotate()", () => {
	expect([1, 2, 3, 4, 5].rotate(1)).toEqual([2, 3, 4, 5, 1]);
});

test("single negative rotate()", () => {
	expect([1, 2, 3, 4, 5].rotate(-1)).toEqual([5, 1, 2, 3, 4]);
});

test("positive rotate()", () => {
	expect([1, 2, 3, 4, 5].rotate(3)).toEqual([4, 5, 1, 2, 3]);
});

test("negative rotate()", () => {
	expect([1, 2, 3, 4, 5].rotate(-3)).toEqual([3, 4, 5, 1, 2]);
});

test("excessive positive rotate()", () => {
	expect([1, 2, 3, 4, 5].rotate(8)).toEqual([4, 5, 1, 2, 3]);
});

test("excessive negative rotate()", () => {
	expect([1, 2, 3, 4, 5].rotate(-8)).toEqual([3, 4, 5, 1, 2]);
});

test("360 positive rotate()", () => {
	expect([1, 2, 3, 4, 5].rotate(5)).toEqual([1, 2, 3, 4, 5]);
});

/*
 * zip
 */

test("left longer zip", () => {
	expect([1, 2, 3].zip([2, 3])).toEqual([[1, 2], [2, 3]]);
});

test("same length zip", () => {
	expect([1, 2, 3].zip([2, 3, 4])).toEqual([[1, 2], [2, 3], [3, 4]]);
});

test("same length zip", () => {
	expect([1, 2].zip([2, 3, 4])).toEqual([[1, 2], [2, 3]]);
});

