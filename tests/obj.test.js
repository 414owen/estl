var obj = require("../obj");
var common = require("./common");

test("peel() on empty object", () => {
	expect(({}).peel()).toEqual([{}]);
});

test("({}).keys() === []", () => {
	expect(({}).keys()).toEqual([]);
});

test("({hello: 2, world: []}).keys() === []", () => {
	expect(({hello: 2, world: []}).keys()).toEqual(["hello", "world"]);
});

test("({hello: 1, yes: \"hello\"}.merge({3: 4, 5: 6}, {6: 7})", () => {
	expect({hello: 1, yes: "hello"}.merge({3: 4, 5: 6}, {6: 7})).toEqual(
		{hello: 1, yes: "hello", 3: 4, 5: 6, 6: 7}
	);
});
