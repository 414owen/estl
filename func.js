/** returns a function that returns a value */
function cnst(val) {
	return function() {return val;};
}

function cnstn(constructor) {
	return function() {return new constructor();};
}

function flip(f) {
	return function(a, b) {
		return f(b, a);
	};
}

function id(a) {return a;}
function fail() {return false;}
function succeed() {return true;}
function call(f) {f();}

module.exports = {
	cnst: cnst,
	cnstn: cnstn,
	flip: flip,
	id: id,
	fail: fail,
	succeed: succeed,
	call: call
};
