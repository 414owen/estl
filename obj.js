require("./arr.js");
var patch = require("./patch")

patch(Object.prototype, [["peelRec", function() {
	switch (this.constructor) {
		case Array:
			return this.deepMap(function(a) {
				return a.peel();
			});
		case Function:
			return this().peel();
	}
	// Necessary so that typeof()s are correct
	return this.constructor(this);
}], ["peel", function() {
	// also evalutes function nodes
	var res = this.peelRec();
	if (Array.isArray(res)) {res = res.deepFlatten();}
	return [res].flatten();
}], ["peelForEach", function(f) {
	this.peel().forEach(f);
}], ["merge", function() {
	var key;
	var res = {};
	for (key in this) {
		res[key] = this[key];
	}
	for (var i = 0; i < arguments.length; i++) {
		var o = arguments[i];
		for (key in o) {
			res[key] = o[key];
		}
	}
	return res;
}], ["keys", function() {
	return Object.keys(this);
}], ["mapKeys", function(f) {
	return this.keys().map(f);
}], ["forKeys", function(f) {
	this.mapKeys(f);
}], ["mapPairs", function(f) {
	var t = this;
	return this.mapKeys(function(k) {
		return f(k, t[k]);
	});
}], ["forPairs", function(f) {
	this.mapPairs(f);
}]]);
