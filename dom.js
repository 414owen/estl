var patch = require("./patch");

function insertWith(w) {
	return function() {
		var t = this;
		Array.from(arguments).deepForEach(function(a) {
			t[w](a.constructor === String ? document.createTextNode(a) : a);
		});
		return t;
	}
}

patch(Element.prototype, [["toggle", function () {
	this.style.display = this.style.display == 'none' ? '' : 'none';
	return this;
}], ["hide", function() {
	this.style.display = "none";
	return this;
}], ["clear", function() {
	while (this.firstChild) {
		this.removeChild(this.firstChild);
	}
	return this;
}], ["append",
	insertWith("appendChild")
], ["prepend",
	insertWith("insertBefore")
], ["replace", function() {
	this.clear().append.apply(this, Array.from(arguments).deepFlatten());
}], ["int", function() {
	return parseInt(this.innerText);
}]]);

patch(Text.prototype, [["replace", function(str) {
	var n = textNode(str);
	this.replaceWith(n);
	return n;
}], ["int", function() {
	return parseInt(this.textContent);
}]]);
