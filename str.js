var patch = require("./patch");

patch(String.prototype, [["reverse", function() {
	return this.split("").reverse().join("");
}], ["lower", String.prototype.toLowerCase
], ["upper", String.prototype.toUpperCase
], ["replaceAll", function(a, b) {
	return this.split(a).join(b);
}], ["contains", function(a) {
	return this.indexOf(a) !== -1;
}], ["casecmp", function(b) {
	// case sensitive compare
	if (this < b) return -1;
	if (this > b) return 1;
	return 0;
}], ["cmp", function(b) {
	// case insensitive compare
	return casecmp(this.lower(), b.lower());
}]]);

patch(String, [["repeat", function(el, times) {
	return Array.repeat(el, times).join("");
}]]);
