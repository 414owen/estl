module.exports = function(root, arr) {
	arr.forEach(function(o) {
		Object.defineProperty(root, o[0], {
			value: o[1],
			enumerable: false,
			writable: true
		});
	});
};
